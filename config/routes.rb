Rails.application.routes.draw do
  devise_for :users


  resources :profiles, only: [:edit, :update, :show] do
    resources :carousels
    resources :hugs
    resources :coffes
    resources :accounts, only: [:update, :create, :index, :destroy]
    # member do
    #       get  'accounts/:id', to: 'accounts#index'
    #     end
  end

  get '/profiles/:profile_id/accounts/:id', to: 'accounts#index', as: :edit_profile_account



  root 'homes#index'


end

# resources :users do
#   resources :certifications, only: [:new, :edit,:create]
#   resources :hobbies
#   member do
#     get 'user_fish', to: 'users#user_fish', as: :fish
#   end
# end
# root 'welcome#show'
