class CreateAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :accounts do |t|
      t.numeric :account_number
      t.numeric :identificacion
      t.string :name
      t.string :email
      t.string :bank_name
      t.string :account_type
      t.references :profile, foreign_key: true

      t.timestamps
    end
  end
end
