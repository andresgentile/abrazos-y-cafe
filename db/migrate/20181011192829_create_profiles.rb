class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles do |t|
      t.string :fullname
      t.string :occupation
      t.text :biography
      t.numeric :amount
      t.string :coin
      t.numeric :multiplier1
      t.numeric :multiplier2
      t.numeric :multiplier3
      t.numeric :multiplier4
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
