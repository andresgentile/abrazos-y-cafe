class CreateCarousels < ActiveRecord::Migration[5.2]
  def change
    create_table :carousels do |t|
      t.text :subtitle
      t.references :profile, foreign_key: true

      t.timestamps
    end
  end
end
