class CreateHugs < ActiveRecord::Migration[5.2]
  def change
    create_table :hugs do |t|
      t.time :time
      t.string :name
      t.string :occupation
      t.text :gratitude
      t.references :profile, foreign_key: true

      t.timestamps
    end
  end
end
