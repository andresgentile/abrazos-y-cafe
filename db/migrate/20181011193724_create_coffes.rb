class CreateCoffes < ActiveRecord::Migration[5.2]
  def change
    create_table :coffes do |t|
      t.numeric :amount
      t.numeric :price
      t.string :name
      t.string :occupation
      t.text :gratitude
      t.references :profile, foreign_key: true

      t.timestamps
    end
  end
end
