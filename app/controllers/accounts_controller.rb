class AccountsController < ApplicationController

  include ActionView::Helpers::UrlHelper

  before_action :set_account, only: [:edit, :update, :destroy]
  before_action :set_profile
  # before_action :set_profile_account

  layout 'barra'

  def index
    @accounts = @profile.accounts.all
    @profile = Profile.find(params[:profile_id])


    if request.path == "/profiles/#{@profile.id}/accounts"
      # request.fullpath.include?(profile_accounts_path)
      @account = @profile.accounts.build
    else
      @account = @profile.accounts.find(params[:id])

    end
  end


  def create
    @account = @profile.accounts.build(account_params)
    @account.save

    if @account.save
        redirect_to profile_accounts_path(@profile)
      else
        redirect_to edit_profile_path(current_user.profile.id)
      end

  end


  def update
      if @account.update(account_params)
        redirect_to profile_accounts_path(@profile)
      else
        redirect_to edit_profile_path(current_user.profile.id)
      end
    end


  def destroy
    @account.destroy
    if @account.destroy
      redirect_to profile_accounts_path(@profile)
    else
      edit_profile_path(current_user.profile.id)
  end
end

  private

    def set_account
      @account = Account.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def account_params
      params.require(:account).permit(:bank_name, :account_number, :name, :identificacion, :email, :profile_id, :id)
    end
    def set_profile_account
      @profile = Account.find(params[:id]).profile
    end

    def set_profile
      @profile = Profile.find(params[:profile_id])
    end


end


