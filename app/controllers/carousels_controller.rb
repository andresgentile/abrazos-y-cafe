class CarouselsController < ApplicationController


  before_action :set_carousel, only: [:show, :edit, :update, :destroy]
  before_action :set_profile

  layout 'barra'



def new
  @carousel =  Carousel.new
  @profile = Profile.find(params[:profile_id])
end

def create
  # @profile = Profile.find(params[:profile_id])
  @carousel = @profile.carousel.build(carousel_params)
  # @carousel = Carousel.new(carousel_params)
  @carousel.save

  if @carousel.save
      redirect_to  profile_carousels_path(@profile)
    else
      redirect_to profile_carousels_path(@profile)
    end

end

def index
  @carousels = Carousel.all
end


def destroy
  @carousel.destroy
  if @carousel.destroy
    redirect_to profile_carousels_path(@profile)
  else
    redirect_to edit_profile_path(current_user.profile.id)
  end
end

def update
  if @carousel.update(carousel_params)
    redirect_to profile_carousels_path(@profile)
  else
    redirect_to edit_profile_path(current_user.profile.id)
  end
end



private

def set_carousel
  @carousel = Carousel.find(params[:id])
end
def set_profile
  @profile = Profile.find(params[:profile_id])
end




def carousel_params
  params.require(:carousel).permit(:subtitle, :img)
end


end
