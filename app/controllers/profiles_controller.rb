class ProfilesController < ApplicationController
  before_action :set_profile, only: [:show, :edit, :update]





def update
    respond_to do |format|
      if @profile.update(profile_params)
        format.html { redirect_to edit_profile_path, notice: 'Profile was successfully updated.' }
        format.json { render :show, status: :ok, location: @profile }
      else
        format.html { render :edit }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
  end

  def edit
    layout 'barra'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_profile
    @profile = Profile.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def profile_params
    params.require(:profile).permit(:fullname, :occupation, :biography, :amount, :coin, :multiplier1, :multiplier2 , :multiplier3, :multiplier4, :img_locura)
  end


end
