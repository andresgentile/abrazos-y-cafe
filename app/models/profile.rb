# == Schema Information
#
# Table name: profiles
#
#  id          :integer          not null, primary key
#  fullname    :string
#  occupation  :string
#  biography   :text
#  amount      :decimal(, )
#  coin        :string
#  multiplier1 :decimal(, )
#  multiplier2 :decimal(, )
#  multiplier3 :decimal(, )
#  multiplier4 :decimal(, )
#  user_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Profile < ApplicationRecord
  belongs_to :user
  has_many :accounts
  has_many :hugs
  has_many :coffes
  has_many :carousel


  has_one_attached :img_locura
  before_create :img_empty

  def img_empty
    unless img_locura.attached?
      self.img_locura.attach(io:File.open(Rails.root.join("app/assets/images/empty_state.svg")), filename: 'empty_state.svg', content_type: "image/svg")
    end
  end

  # def push_name
  #   if current_user.profile.fullname?
  #     current_user.email
  #   else
  #     current_user.profile.fullname
  # end


end
