# == Schema Information
#
# Table name: hugs
#
#  id         :integer          not null, primary key
#  time       :time
#  name       :string
#  occupation :string
#  gratitude  :text
#  profile_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Hug < ApplicationRecord
  belongs_to :profile
end
