# == Schema Information
#
# Table name: carousels
#
#  id         :integer          not null, primary key
#  subtitle   :text
#  profile_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Carousel < ApplicationRecord
  belongs_to :profile
  has_one_attached :img
end
