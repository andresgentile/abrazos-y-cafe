# == Schema Information
#
# Table name: accounts
#
#  id             :integer          not null, primary key
#  account_number :decimal(, )
#  identificacion :decimal(, )
#  name           :string
#  email          :string
#  bank_name      :string
#  account_type   :string
#  profile_id     :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Account < ApplicationRecord
  belongs_to :profile
end
