# == Schema Information
#
# Table name: coffes
#
#  id         :integer          not null, primary key
#  amount     :decimal(, )
#  price      :decimal(, )
#  name       :string
#  occupation :string
#  gratitude  :text
#  profile_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Coffe < ApplicationRecord
  belongs_to :profile
end
